
## Incorporating Pre-Training in Long Short-Term Memory Networks for Tweets Classification

Code for paper "Incorporating Pre-Training in Long Short-Term Memory Networks for Tweets Classification"

### Training Data

There are four files in the **Data** folder:

* discrimination.txt: the well-labeled discrimination related tweets
* non-discrimination.txt: the well-labeled non-discrimination related tweets
* weakly-discrimination.txt: the weakly-labeled discrimination related tweets predicted by Naive Bayes Classifier
* weakly-non-discrimination.txt: the weakly-labeled non-discrimination related tweets predicted by Naive Bayes Classifier

### Requirements

Code is written in Python (2.7) and requires Theano (0.7)

The pre-trained word2vec need to be downloaded from [here](https://code.google.com/p/word2vec/) and added to the **Data** folder.

### Data Pre-processing

To pre-process the raw data, run


`python util.py`

This will create a pickle file `dis.p` int the folder.

### Running the models

`python pre_train_lstm.py`

This will run the LSTM model with pre-training and use 20% of the well-labeled dataset as test data.
If the parameter `use_pretrain` change from `True` to `False` in the source code, the LSTM model would be trained without pre-training