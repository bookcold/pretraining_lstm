from tokenize import tokenize
from nltk.corpus import stopwords
from gensim.models.word2vec import Word2Vec
from collections import OrderedDict
from random import shuffle
import numpy as np
import cPickle
import string
import os


def load_data():
    positive_corpus, negative_corpus = find_tweets()
    weakly_pos_corpus, weakly_neg_corpus = find_weakly_tweets(num=5000)
    word_count_dict = build_dictionary(positive_corpus+negative_corpus+weakly_pos_corpus+weakly_neg_corpus)
    w2v = load_bin_vec(word_count_dict)
    print "num words already in word2vec: " + str(len(w2v))
    add_unknown_words(w2v, word_count_dict, k=300)
    W, word_idx_map = get_W(w2v, word_count_dict, k=300)
    pos = text_to_int(positive_corpus, word_idx_map)
    neg = text_to_int(negative_corpus, word_idx_map)
    weakly_pos = text_to_int(weakly_pos_corpus, word_idx_map)
    weakly_neg = text_to_int(weakly_neg_corpus, word_idx_map)
    assert len(pos) == len(neg)
    pre_train = (weakly_pos, weakly_neg)
    data = (pos, neg)
    cPickle.dump([data, pre_train, W, word_idx_map, word_count_dict], open('dis.p','wb'))


def find_tweets():
    clean_pos = []
    clean_neg = []
    with open('Data/discrimination.txt') as f:
        positive_corpus = f.read().splitlines()
    with open('Data/non-discrimination.txt') as f:
        negative_corpus = f.read().splitlines()
    for tweet in positive_corpus:
        text = tweet_tokenzie(tweet)
        clean_pos.append(text)
    for tweet in negative_corpus:
        text = tweet_tokenzie(tweet)
        clean_neg.append(text)
    assert len(clean_neg) == len(clean_pos)
    print 'num of golden set: ', len(clean_neg)*2
    return clean_pos, clean_neg


def find_weakly_tweets(len_tweet=5, num=5000):
    clean_weakly_pos = []
    clean_weakly_neg = []
    with open('Data/weakly-discrimination.txt') as f:
        positive_corpus = f.read().splitlines()
    with open('Data/weakly-non-discrimination.txt') as f:
        negative_corpus = f.read().splitlines()
    shuffle(positive_corpus)
    shuffle(negative_corpus)
    for tweet in positive_corpus:
        text = tweet_tokenzie(tweet)
        if len(text) > len_tweet:
            clean_weakly_pos.append(text)
        if len(clean_weakly_pos) == num:
            break
    for tweet in negative_corpus:
        text = tweet_tokenzie(tweet)
        if len(text) > len_tweet:
            clean_weakly_neg.append(tweet_tokenzie(tweet))
        if len(clean_weakly_pos) == len(clean_weakly_neg):
            break
    assert len(clean_weakly_pos) == len(clean_weakly_neg)
    print 'num of weakly labeled set: ', len(clean_weakly_pos)*2
    return clean_weakly_pos, clean_weakly_neg


def tweet_tokenzie(tweet):
    key_words=['discrimination','breaking','news','sexism','racism','blackoncampus','everydaysexism']
    text = []
    tweet = tokenize(tweet)
    for t in reversed(tweet):
        if (t.startswith('#')):
            tweet.pop()
        else:
            break
    for word in tweet:
        if (word in string.punctuation) or (word in stopwords.words('english') or (word.startswith('http')))\
                or (word.startswith('RT')) or (word.startswith('@')):
            continue
        if word.startswith('#'):
            if word[1:].lower() in key_words:
                continue
            else:
                text.append(word[1:].lower())
        else:
            text.append(word.lower())
    return text


def build_dictionary(text):
    print 'Building dictionary'
    word_count = dict()
    for sentence in text:
        for word in sentence:
            if word not in word_count:
                word_count[word] = 1
            else:
                word_count[word] += 1
    counts = word_count.values()
    keys = word_count.keys()
    print np.sum(counts), ' total words', len(keys), ' unique words'
    return word_count


def text_to_int(text, dictionary, num=None):
    new_dictionary = dict()
    if num != None:
        i = 0
        for key in dictionary:
            if i < num:
                new_dictionary[key] = dictionary[key]
    else:
        new_dictionary = dictionary
    seqs = [None] * len(text)
    for idx, sentence in enumerate(text):
        # seqs[idx] = [dictionary[w] if w in dictionary else 1 for w in sentence]
        seqs[idx] = [new_dictionary[w] for w in sentence if w in new_dictionary]
    return seqs


def shuffle_data(x, y):
    new_x = []
    new_y = []
    assert len(x) == len(y)
    indexes = range(len(x))
    np.random.shuffle(indexes)
    for index in indexes:
        new_x.append(x[index])
        new_y.append(y[index])
    return new_x, new_y

def load_bin_vec(vocab):
    """
    Loads 300x1 word vecs from Google (Mikolov) word2vec
    """
    fname = 'Data/GoogleNews-vectors-negative300.bin'
    word_vecs = {}
    with open(fname, "rb") as f:
        header = f.readline()
        vocab_size, layer1_size = map(int, header.split())
        binary_len = np.dtype('float32').itemsize * layer1_size
        for line in xrange(vocab_size):
            word = []
            while True:
                ch = f.read(1)
                if ch == ' ':
                    word = ''.join(word)
                    break
                if ch != '\n':
                    word.append(ch)
            if word in vocab:
               word_vecs[word] = np.fromstring(f.read(binary_len), dtype='float32')
            else:
                f.read(binary_len)
    return word_vecs


def add_unknown_words(word_vecs, vocab, min_df=5, k=300):
    """
    For words that occur in at least min_df documents, create a separate word vector.
    0.25 is chosen so the unknown vectors have (approximately) same variance as pre-trained ones
    """
    count = 0
    for word in vocab:
        if word not in word_vecs and vocab[word] >= min_df:
            word_vecs[word] = np.random.uniform(-0.25,0.25,k)
            count += 1
    print 'unkown words number is: ', count


def get_W(word_vecs, word_count_dict, k=100):
    """
    Get word matrix. W[i] is the vector for word indexed by i
    """
    vocab_size = len(word_vecs)
    word_idx_map = OrderedDict()
    W = np.zeros(shape=(vocab_size+1, k))
    W[0] = np.zeros(k)
    i = 1
    for word in sorted(word_count_dict, key=word_count_dict.get, reverse=True):
        if word in word_vecs:
            W[i] = word_vecs[word]
            word_idx_map[word] = i
            i += 1
    return W, word_idx_map

if __name__ == '__main__':
    load_data()