"""
Training code for
A Long Short-Term Memory Network for Tweets Classification based on Pre-training

Some the code is modified from
- http://deeplearning.net/tutorial/lstm.html
- https://github.com/yoonkim/CNN_sentence
"""

from collections import OrderedDict
import cPickle as pkl
import numpy
from sklearn import cross_validation
import theano
from theano import config
import theano.tensor as tensor
from theano.ifelse import ifelse
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
from datetime import datetime
SEED = 1024
numpy.random.seed(SEED)


def numpy_floatX(data):
    return numpy.asarray(data, dtype=config.floatX)


def ReLU(x):
    y = tensor.maximum(0.0, x)
    return(y)


def get_minibatches_idx(n, minibatch_size, shuffle=False):
    idx_list = numpy.arange(n, dtype="int32")
    if shuffle:
        numpy.random.shuffle(idx_list)
    minibatches = []
    minibatch_start = 0
    for i in range(n // minibatch_size):
        minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
        minibatch_start += minibatch_size
    if (minibatch_start != n):
        # Make a minibatch out of what is left
        minibatches.append(idx_list[minibatch_start:])
    return zip(range(len(minibatches)), minibatches)


def zipp(params, tparams):
    """
    When we reload the model. Needed for the GPU stuff.
    """
    for kk, vv in params.iteritems():
        tparams[kk].set_value(vv)


def unzip(zipped):
    """
    When we pickle the model. Needed for the GPU stuff.
    """
    new_params = OrderedDict()
    for kk, vv in zipped.iteritems():
        new_params[kk] = vv.get_value()
    return new_params


def dropout_layer(state_before, use_noise, trng):
    proj = tensor.switch(use_noise,
                         (state_before *
                          trng.binomial(state_before.shape,
                                        p=0.5, n=1,
                                        dtype=state_before.dtype)),
                         state_before * 0.5)
    return proj


def _p(pp, name):
    return '%s_%s' % (pp, name)


def init_params(options):
    params = OrderedDict()
    params = param_init_lstm(options, params, prefix='lstm')
    # classifier parameters
    params['U'] = 0.01 * numpy.random.randn(options['dim_proj'],
                                            options['ydim']).astype(config.floatX)
    params['b'] = numpy.zeros((options['ydim'],)).astype(config.floatX)
    return params


def load_params(path, params):
    pp = numpy.load(path)
    for kk, vv in params.iteritems():
        if kk not in pp:
            raise Warning('%s is not in the archive' % kk)
        params[kk] = pp[kk]
    return params


def init_tparams(params):
    tparams = OrderedDict()
    for kk, pp in params.iteritems():
        tparams[kk] = theano.shared(params[kk], name=kk)
    return tparams


def ortho_weight(ndim):
    W = numpy.random.randn(ndim, ndim)
    u, s, v = numpy.linalg.svd(W)
    return u.astype(config.floatX)


def param_init_lstm(options, params, prefix='lstm'):
    W = numpy.concatenate([ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj'])], axis=1)
    params[_p(prefix, 'W')] = W
    U = numpy.concatenate([ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj'])], axis=1)
    params[_p(prefix, 'U')] = U
    b = numpy.zeros((4 * options['dim_proj'],))
    params[_p(prefix, 'b')] = b.astype(config.floatX)
    return params


def lstm_layer(tparams, state_below, options, prefix='lstm', mask=None):
    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1

    assert mask is not None

    def _slice(_x, n, dim):
        if _x.ndim == 3:
            return _x[:, :, n * dim:(n + 1) * dim]
        return _x[:, n * dim:(n + 1) * dim]

    def _step(m_, x_, h_, c_):
        preact = tensor.dot(h_, tparams[_p(prefix, 'U')])
        preact += x_

        i = tensor.nnet.sigmoid(_slice(preact, 0, options['dim_proj']))
        f = tensor.nnet.sigmoid(_slice(preact, 1, options['dim_proj']))
        o = tensor.nnet.sigmoid(_slice(preact, 2, options['dim_proj']))
        c = tensor.tanh(_slice(preact, 3, options['dim_proj']))

        c = f * c_ + i * c
        c = m_[:, None] * c + (1. - m_)[:, None] * c_

        h = o * tensor.tanh(c)
        h = m_[:, None] * h + (1. - m_)[:, None] * h_

        return h, c

    state_below = (tensor.dot(state_below, tparams[_p(prefix, 'W')]) +
                   tparams[_p(prefix, 'b')])

    dim_proj = options['dim_proj']
    rval, updates = theano.scan(_step,
                                sequences=[mask, state_below],
                                outputs_info=[tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj),
                                              tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj)],
                                name=_p(prefix, '_layers'),
                                n_steps=nsteps)
    return rval[0]


def mean_layer(input, options):
    return tensor.mean(input, axis=0).reshape((1, options['dim_proj']))


def matric_learning(pos, neg, twitter, options):
    pos = tensor.tile(pos, (options['batch_size'], 1))
    neg = tensor.tile(neg, (options['batch_size'], 1))
    is_tw_pos = tensor.scalar('is_tw_pos')
    sim_pos = tensor.sum(pos * twitter, axis=1) / ((tensor.sqrt(tensor.sum(tensor.sqr(pos), 1) + 1e-5))
                                                   *(tensor.sqrt(tensor.sum(tensor.sqr(twitter), 1) + 1e-5)))
    sim_neg = tensor.sum(neg * twitter, axis=1) / ((tensor.sqrt(tensor.sum(tensor.sqr(neg), 1) + 1e-5))
                                                   *(tensor.sqrt(tensor.sum(tensor.sqr(twitter), 1) + 1e-5)))
    cost_pos = tensor.log(1+tensor.exp(-options['gamma']*(sim_pos-sim_neg))).mean()
    cost_neg = tensor.log(1+tensor.exp(-options['gamma']*(sim_neg-sim_pos))).mean()
    cost = ifelse(is_tw_pos, cost_pos, cost_neg)
    return is_tw_pos, cost


def build_golden_model(tparams, options):
    x, mask, proj = build_sentence_model(tparams, options)
    proj = proj.reshape([options['tw_batch'], options['dim_proj']])
    output = mean_layer(proj, options)
    return x, mask, output


def build_pre_train_model(tparams, options):
    gold_pos, gold_pos_mask, pos_feature = build_golden_model(tparams, options)
    gold_neg, gold_neg_mask, neg_feature = build_golden_model(tparams, options)
    x, mask, proj = build_sentence_model(tparams, options)
    is_tw_pos, cost = matric_learning(pos_feature, neg_feature, proj, options)
    return gold_pos, gold_pos_mask, gold_neg, gold_neg_mask, x, mask, cost, is_tw_pos


def build_sentence_model(tparams, options):
    x = tensor.matrix('x', dtype='int64')
    mask = tensor.matrix('mask', dtype=config.floatX)

    n_timesteps = x.shape[0]
    n_samples = x.shape[1]

    emb = tparams['Wemb'][x.flatten()].reshape([n_timesteps,
                                                n_samples,
                                                options['dim_proj']])
    proj = lstm_layer(tparams, emb, options, prefix='lstm', mask=mask)

    proj = (proj * mask[:, :, None]).sum(axis=0)
    proj = proj / mask.sum(axis=0)[:, None]
    return x, mask, proj


def build_classify_model(tparams, options):
    x, mask, proj = build_sentence_model(tparams, options)
    n_samples = proj.shape[0]
    y = tensor.vector('y', dtype='int64')
    trng = RandomStreams(SEED)
    use_noise = theano.shared(numpy_floatX(0.))
    if options['use_dropout']:
        proj = dropout_layer(proj, use_noise, trng)
    pred = tensor.nnet.softmax(tensor.dot(proj, tparams['U']) + tparams['b'])
    f_pred = theano.function([x, mask], pred.argmax(axis=1), name='f_pred')
    cost = -tensor.log(pred[tensor.arange(n_samples), y] + 1e-8).mean()
    return use_noise, f_pred, cost, x, mask, y


def adadelta(lr, tparams, grads, gold_pos, gold_pos_mask, gold_neg, gold_neg_mask, x, mask, cost, is_tw_pos):
    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                  name='pre_%s_grad' % k, broadcastable=p.broadcastable)
                    for k, p in tparams.iteritems()]
    running_up2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                 name='pre_%s_rup2' % k, broadcastable=p.broadcastable)
                   for k, p in tparams.iteritems()]
    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                    name='pre_%s_rgrad2' % k, broadcastable=p.broadcastable)
                      for k, p in tparams.iteritems()]

    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
    rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
             for rg2, g in zip(running_grads2, grads)]

    f_grad_shared = theano.function([gold_pos, gold_pos_mask, gold_neg, gold_neg_mask, x, mask, is_tw_pos], cost, updates=zgup + rg2up,
                                    name='adadelta_f_grad_shared')

    updir = [-tensor.sqrt(ru2 + 1e-6) / tensor.sqrt(rg2 + 1e-6) * zg
             for zg, ru2, rg2 in zip(zipped_grads,
                                     running_up2,
                                     running_grads2)]
    ru2up = [(ru2, 0.95 * ru2 + 0.05 * (ud ** 2))
             for ru2, ud in zip(running_up2, updir)]
    param_up = [(p, p + ud) for p, ud in zip(tparams.values(), updir)]

    f_update = theano.function([lr], [], updates=ru2up + param_up,
                               on_unused_input='ignore',
                               name='adadelta_f_update')

    return f_grad_shared, f_update


def classify_adadelta(lr, tparams, grads, cost, x, x_mask, y):
    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                  name='%s_grad' % k, broadcastable=p.broadcastable)
                    for k, p in tparams.iteritems()]
    running_up2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                 name='%s_rup2' % k, broadcastable=p.broadcastable)
                   for k, p in tparams.iteritems()]
    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                    name='%s_rgrad2' % k, broadcastable=p.broadcastable)
                      for k, p in tparams.iteritems()]

    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
    rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
             for rg2, g in zip(running_grads2, grads)]

    f_grad_shared = theano.function([x, x_mask, y], cost, updates=zgup + rg2up,
                                    name='classify_adadelta_f_grad_shared')

    updir = [-tensor.sqrt(ru2 + 1e-6) / tensor.sqrt(rg2 + 1e-6) * zg
             for zg, ru2, rg2 in zip(zipped_grads,
                                     running_up2,
                                     running_grads2)]
    ru2up = [(ru2, 0.95 * ru2 + 0.05 * (ud ** 2))
             for ru2, ud in zip(running_up2, updir)]
    param_up = [(p, p + ud) for p, ud in zip(tparams.values(), updir)]

    f_update = theano.function([lr], [], updates=ru2up + param_up,
                               on_unused_input='ignore',
                               name='classify_adadelta_f_update')
    return f_grad_shared, f_update


def train_model(
    dataset,
    dim_proj=300,  # word embeding dimension and LSTM number of hidden units.
    patience=10,  # Number of epoch to wait before early stop if no progress
    max_pretrain_epochs=20,
    max_epochs=5000,  # The maximum number of epoch to run
    lrate=0.0001,  # Learning rate for sgd (not used for adadelta and rmsprop)
    gamma=1,
    maxlen=15,  # Sequence longer then this get ignored
    tw_batch=100,
    batch_size=30,  # The batch size during training.
    use_dropout=True,  # if False slightly faster, but worst test error
                       # This frequently need a bigger model.
    use_pretrain=True,
    test_portion=0.3,
    cv=5
):
    # Model options
    model_options = locals().copy()
    model_options.pop('dataset', None)
    model_options['ydim'] = 2
    print "model options", model_options

    print 'Load training data'
    data, pre_train, Wemb, word_idx_map, vocab = dataset[0], dataset[1], dataset[2], dataset[3], dataset[4]
    pos, neg = data

    print "prepare the training and testing data"
    results = []
    kf = cross_validation.ShuffleSplit(len(pos), n_iter=cv, test_size=test_portion, random_state=2048)
    for i, (train_index, test_index) in enumerate(kf):
        print 'cross fold: ', i
        print "init parameters"
        params = init_params(model_options)
        params['Wemb'] = Wemb.astype(config.floatX)
        tparams = init_tparams(params)

        lr = tensor.scalar(name='lr')
        if use_pretrain:
            print "build pre train model"
            gold_pos, gold_pos_mask, gold_neg, gold_neg_mask, x, mask, pretrain_cost, is_tw_pos = \
                build_pre_train_model(tparams, model_options)
            f_cost = theano.function([gold_pos, gold_pos_mask, gold_neg, gold_neg_mask, x, mask, is_tw_pos], pretrain_cost, name='f_cost')
            grads = tensor.grad(pretrain_cost, wrt=tparams.values(), disconnected_inputs='ignore')
            f_grad_shared, f_update = adadelta(lr, tparams, grads,
                                                gold_pos, gold_pos_mask, gold_neg, gold_neg_mask, x, mask, pretrain_cost, is_tw_pos)

        print "build classifier model"
        use_noise, f_pred, classify_cost, gold_x, gold_mask, gold_y = build_classify_model(tparams, model_options)
        classify_grads = tensor.grad(classify_cost, wrt=tparams.values(), disconnected_inputs='ignore')
        classify_f_grad_shared, classify_f_update = classify_adadelta(lr, tparams, classify_grads, classify_cost, gold_x, gold_mask, gold_y)

        [(gold_pos_x, gold_neg_x), (test_pos, test_neg)] = make_idx_data_gold_pre(pos, neg, train_index, test_index, tw_batch)
        train_x = numpy.array(gold_pos_x + gold_neg_x)
        train_y = numpy.array([1] * len(gold_pos_x) + [0] * len(gold_neg_x))
        test_x = numpy.array(test_pos + test_neg)
        test_y = numpy.array([1] * len(test_pos) + [0] * len(test_neg))

        (pre_pos_x, pre_neg_x) = pre_train
        assert len(train_x) == len(train_y)
        assert len(test_x) == len(test_y)
        p = numpy.random.permutation(len(train_x))
        train_x = train_x[p]
        train_y = train_y[p]
        p = numpy.random.permutation(len(test_x))
        test_x = test_x[p]
        test_y = test_y[p]
        pre_train_x, pre_train_mask, pre_train_y = prepare_data(train_x, train_y, maxlen=maxlen)
        test_x, test_mask, test_y = prepare_data(test_x, test_y, maxlen=maxlen)

        print "prepare golden model"
        gold_pos_x, gold_pos_mask_x, _ = prepare_data(gold_pos_x, len(gold_pos_x)*[0], maxlen=maxlen)
        gold_neg_x, gold_neg_mask_x, _ = prepare_data(gold_neg_x, len(gold_neg_x)*[0], maxlen=maxlen)

        print "pre-train"
        estop = False
        best_pre_err = numpy.inf
        uidx = 0  # the number of update done
        best_p = None
        bad_count = 0
        if use_pretrain:
            for eidx in xrange(max_pretrain_epochs):
                print 'epoch: ', eidx
                kf_pos = get_minibatches_idx(len(pre_pos_x), batch_size, shuffle=True)
                kf_neg = get_minibatches_idx(len(pre_neg_x), batch_size, shuffle=True)
                for (_, pos_index), (_, neg_index) in zip(kf_pos[:-1], kf_neg[:-1]):
                    uidx += 1
                    mini_pos_x = [pre_pos_x[t] for t in pos_index]
                    mini_pos_x, mini_pos_mask, _ = prepare_data(mini_pos_x, len(mini_pos_x)*[0], maxlen=maxlen)
                    mini_neg_x = [pre_neg_x[t] for t in neg_index]
                    mini_neg_x, mini_neg_mask, _ = prepare_data(mini_neg_x, len(mini_neg_x)*[0], maxlen=maxlen)
                    is_tw_pos = 1
                    positive_cost = f_grad_shared(gold_pos_x, gold_pos_mask_x, gold_neg_x, gold_neg_mask_x, mini_pos_x,
                                                  mini_pos_mask, is_tw_pos)
                    f_update(lrate)
                    is_tw_pos = 0
                    negative_cost = f_grad_shared(gold_pos_x, gold_pos_mask_x, gold_neg_x, gold_neg_mask_x, mini_neg_x,
                                                  mini_neg_mask, is_tw_pos)
                    f_update(lrate)
                    if numpy.isnan(positive_cost) or numpy.isinf(positive_cost)\
                            or numpy.isnan(negative_cost) or numpy.isnan(negative_cost):
                        print 'NaN detected'
                        return 1., 1., 1.
                    if numpy.mod(uidx, 30) == 0:
                        print str(datetime.now()), ' Epoch ', eidx, ' Update ', uidx, ' Positive Cost ', positive_cost, \
                            ' Negative Cost ', negative_cost
                use_noise.set_value(0.)
                train_err = pred_error(f_pred, [pre_train_x, pre_train_mask, pre_train_y])
                if best_p is None or train_err < best_pre_err:
                    best_p = unzip(tparams)
                    best_pre_err = train_err
                    bad_count = 0
                else:
                    bad_count += 1
                    if bad_count > patience:
                        estop = True
                if estop:
                    print 'Early Stop'
                    break

        print "classification"
        if use_pretrain and best_p is not None:
            zipp(best_p, tparams)
        history_errs = []
        best_p = None
        estop = False
        bad_count = 0
        best_err = 0
        for eidx in xrange(max_epochs):
            minibatches = get_minibatches_idx(len(train_x), batch_size, shuffle=True)
            for (_, index) in minibatches:
                mini_train_x = [train_x[t] for t in index]
                mini_train_y = [train_y[t] for t in index]
                mini_train_x, mini_train_mask, mini_train_y = prepare_data(mini_train_x, mini_train_y, maxlen=maxlen)
                use_noise.set_value(1.)
                cost = classify_f_grad_shared(mini_train_x, mini_train_mask, mini_train_y)
                classify_f_update(lrate)
                if numpy.isnan(cost) or numpy.isinf(cost)\
                        or numpy.isnan(cost) or numpy.isnan(cost):
                    print 'NaN detected'
                    return 1., 1., 1.
            use_noise.set_value(0.)
            train_err = pred_error(f_pred, [pre_train_x, pre_train_mask, pre_train_y])
            test_err = pred_error(f_pred, [test_x, test_mask, test_y])
            history_errs.append(test_err)
            if best_p is None or test_err < best_err:
                best_p = unzip(tparams)
                best_err = test_err
                bad_count = 0
            else:
                bad_count += 1
                if bad_count > patience or train_err == 0:
                    estop = True
            if estop:
                break
        print 'best test error at cv %i: %f' %(i, best_err)
        results.append(best_err)
    print 'the final test error is: ', numpy.mean(results)


def pred_probs(f_pred_prob, prepare_data, data, iterator, maxlen):
    """ If you want to use a trained model, this is useful to compute
    the probabilities of new examples.
    """
    n_samples = len(data[0])
    probs = numpy.zeros((n_samples, 2)).astype(config.floatX)
    for _, valid_index in iterator:
        x, mask, y = prepare_data([data[0][t] for t in valid_index],
                                  numpy.array(data[1])[valid_index],
                                  maxlen=maxlen)
        pred_probs = f_pred_prob(x, mask)
        probs[valid_index, :] = pred_probs
    return probs


def pred_error(f_pred, data):
    """
    Just compute the error
    f_pred: Theano fct computing the prediction
    prepare_data: usual prepare_data for that dataset.
    """
    valid_err = 0
    [x, mask, y] = data
    preds = f_pred(x, mask)
    targets = numpy.array(y)
    valid_err += (preds == targets).sum()
    valid_err = 1. - numpy_floatX(valid_err) / len(preds)
    return valid_err


def make_idx_data_gold_pre(pos, neg, train_index, test_index, gold_num):
    assert gold_num <= len(train_index)
    train_index = train_index[:gold_num]
    gold_pos = [pos[s] for s in train_index]
    gold_neg = [neg[s] for s in train_index]
    test_pos = [pos[s] for s in test_index]
    test_neg = [neg[s] for s in test_index]
    return [(gold_pos, gold_neg), (test_pos, test_neg)]


def prepare_data(seqs, labels, maxlen=None):
    """Create the matrices from the datasets.

    This pad each sequence to the same lenght: the lenght of the
    longuest sequence or maxlen.

    if maxlen is set, we will cut the sequence to fit the maximum
    lenght.

    This swap the axis!
    """
    # x: a list of sentences
    lengths = [len(s) for s in seqs]

    if maxlen is not None:
        new_seqs = []
        new_labels = []
        new_lengths = []
        for l, s, y in zip(lengths, seqs, labels):
            if l < maxlen:
                new_seqs.append(s)
                new_labels.append(y)
                new_lengths.append(l)
            else:
                new_seqs.append(s[:maxlen])
                new_labels.append(y)
                new_lengths.append(maxlen)
        lengths = new_lengths
        labels = new_labels
        seqs = new_seqs

        if len(lengths) < 1:
            return None, None, None

    n_samples = len(seqs)
    maxlen = numpy.max(lengths)

    x = numpy.zeros((maxlen, n_samples)).astype('int64')
    x_mask = numpy.zeros((maxlen, n_samples)).astype(numpy.float32)
    for idx, s in enumerate(seqs):
        x[:lengths[idx], idx] = s
        x_mask[:lengths[idx], idx] = 1.
    return x, x_mask, labels


def main():
    test_portion = 0.2
    use_pretrain = True
    tw_batch = 300 * (1 - test_portion)

    print "loading data..."
    dataset = pkl.load(open("dis.p", "rb"))
    train_model(
        dataset,
        tw_batch=int(tw_batch),
        max_epochs=100,
        max_pretrain_epochs=30,
        patience=10,
        test_portion=test_portion,
        gamma=10,
        cv=5,
        use_pretrain=use_pretrain
    )

if __name__ == '__main__':
    main()